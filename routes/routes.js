const service = require('../controllers/service')

module.exports = (app) => {
    app.get('/api/getNewAssignments', service.getNewAssignments)//for new assignments that staff submitted
    app.get('/api/getStudentCourses', service.getStudentCourses)//for which courses studying by a student
    app.get('/api/getStudents', service.getStudents)//students under staff courses

    //other featured routes
    app.get('/api/getCourses', service.getCourses)
    app.get('/api/getAssignments', service.getAssignments)
    app.post('/api/insertStudent', service.insertStudent)
    app.post('/api/crateAssignment', service.createAssignment)
}
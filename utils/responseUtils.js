var ResponseUtils = module.exports

ResponseUtils.Response = function (api_response) {
    let error_object = {}
    error_object.status_code = 400
    error_object.status = 'ERR'
    error_object.type = 'ERROR'

    let success_object = {}
    success_object.status_code = 200
    success_object.status = 'SUCCESS'
    success_object.type = 'SUCCESS'

    this.error = function (message, status, status_code) {
        error_object.message = message
        error_object.status = status
        error_object.status_code = status_code || error_object.status_code
        api_response.status(error_object.status_code).send(error_object)
    }

    this.success = function (message, data, status) {
        success_object.message = message
        success_object.status = status || success_object.status
        data || data == 0 ? success_object.data = data : 'no data'
        api_response.status(200).send(success_object)
    }
}
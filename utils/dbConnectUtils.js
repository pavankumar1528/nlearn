const mysql = require('mysql')
const config = require('config')



const DbConnectUtils = module.exports
const connectionPools={};

const getConnectionPool = (dbName) => {
    if (connectionPools[dbName] != null) {
        return connectionPools[dbName]
    }

    const dbPool = mysql.createPool({
        connectionLimit: config.mySqldb[dbName].connectionLimit,
        waitForConnections: config.mySqldb[dbName].waitForConnections,
        host: config.mySqldb[dbName].hostname,
        user: config.mySqldb[dbName].username,
        password: config.mySqldb[dbName].password,
        database: config.mySqldb[dbName].database,
        port: config.mySqldb[dbName].port
    })
    connectionPools[dbName] = dbPool
    return dbPool
}

DbConnectUtils.getMySqlConn = function (db) {
    return new Promise((resolve, reject) => {
        db.getConnection((err, connection) => {
            if (connection) { return resolve(connection) }
            reject(err)
        })
    })
}

DbConnectUtils.execSqlQry = function (qry, params, db) {
    db = getConnectionPool(db)
    return new Promise((resolve, reject) => {
        DbConnectUtils.getMySqlConn(db)
            .then(connection => {
                connection.query(qry, params, (qryErr, qryResp) => {
                    connection.release()
                    if (qryResp) {
                        return resolve({
                            message: 'SUCCESS',
                            success: true,
                            data: qryResp
                        })
                    }
                    reject({
                        message: qryErr,
                        success: false,
                        data: []
                    })
                })
            })
            .catch(err => {
                reject({
                    message: err,
                    success: false,
                    data: []
                })
            })
    })
}
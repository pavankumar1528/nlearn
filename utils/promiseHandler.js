module.exports = Promise => {
    return Promise.then(data => {
        return [null, data];
    })
    .catch(Err => {
        console.error(Err);
        return [Err];
    })
}
CREATE DATABASE  IF NOT EXISTS `nLearn_assignment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nLearn_assignment`;
-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 0.0.0.0    Database: nLearn_assignment
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assignments`
--

DROP TABLE IF EXISTS `assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignments` (
  `asn_id` int(11) NOT NULL AUTO_INCREMENT,
  `crs_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `stf_id` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `submitted_date` date DEFAULT NULL,
  PRIMARY KEY (`asn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignments`
--

LOCK TABLES `assignments` WRITE;
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT INTO `assignments` VALUES (1,1,'datatpes','6','2020-01-20','2020-01-25','2020-01-17'),(2,1,'data_structures','5','2020-01-23','2020-01-28','2020-01-22'),(3,5,'networking','6','2020-02-06','2020-02-10','2020-02-04'),(4,6,'text_summary','7','2020-01-15','2020-01-20','2020-01-12'),(5,8,'objects','8','2020-01-13','2020-01-18','2020-01-13'),(6,2,'classes','5','2020-01-04','2020-01-08','2020-01-03');
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_offered`
--

DROP TABLE IF EXISTS `courses_offered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_offered` (
  `crs_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`crs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_offered`
--

LOCK TABLES `courses_offered` WRITE;
/*!40000 ALTER TABLE `courses_offered` DISABLE KEYS */;
INSERT INTO `courses_offered` VALUES (1,'C'),(2,'Java'),(3,'Dbms'),(4,'Auto_cad'),(5,'Python'),(6,'ML'),(7,'NLP'),(8,'JS');
/*!40000 ALTER TABLE `courses_offered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_scheduled`
--

DROP TABLE IF EXISTS `courses_scheduled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_scheduled` (
  `crs_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`crs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_scheduled`
--

LOCK TABLES `courses_scheduled` WRITE;
/*!40000 ALTER TABLE `courses_scheduled` DISABLE KEYS */;
INSERT INTO `courses_scheduled` VALUES (1,'C','2020-01-05','2020-03-27'),(2,'Java','2020-01-02','2020-04-06'),(3,'Dbms','2020-01-12','0202-02-16'),(5,'Python','2020-01-06','2020-02-28'),(6,'ML','2020-02-02','2020-04-15'),(7,'NLP','2020-02-06','2020-04-16'),(8,'JS','2020-01-10','2020-03-06');
/*!40000 ALTER TABLE `courses_scheduled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `placements`
--

DROP TABLE IF EXISTS `placements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placements` (
  `placement_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) NOT NULL,
  `drive_date` date NOT NULL,
  `academic_year` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`placement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `placements`
--

LOCK TABLES `placements` WRITE;
/*!40000 ALTER TABLE `placements` DISABLE KEYS */;
/*!40000 ALTER TABLE `placements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semesters`
--

DROP TABLE IF EXISTS `semesters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semesters` (
  `sem_id` int(11) NOT NULL AUTO_INCREMENT,
  `degree` varchar(20) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `semester` int(2) NOT NULL,
  `academic_year` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`sem_id`,`degree`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semesters`
--

LOCK TABLES `semesters` WRITE;
/*!40000 ALTER TABLE `semesters` DISABLE KEYS */;
/*!40000 ALTER TABLE `semesters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `stf_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `courses` json DEFAULT NULL,
  PRIMARY KEY (`stf_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (5,'chandu','chn@gmail.com','[1, 2]'),(6,'naishitha','az@gmail.com','[3, 5, 1]'),(7,'sunil','snl@gmail.com','[6, 7]'),(8,'hari','hari@gmail.com','[1, 2, 5, 8]'),(9,'surendra','srn@gmail.com','[4]');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_assignments`
--

DROP TABLE IF EXISTS `student_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_assignments` (
  `stu_id` int(11) NOT NULL,
  `asn_id` int(11) NOT NULL,
  `stf_id` int(11) NOT NULL,
  `asn_progress` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stu_id`,`asn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_assignments`
--

LOCK TABLES `student_assignments` WRITE;
/*!40000 ALTER TABLE `student_assignments` DISABLE KEYS */;
INSERT INTO `student_assignments` VALUES (1,3,6,0),(1,8,8,1),(2,2,5,0),(4,3,6,0),(6,8,8,0),(13,6,5,1),(15,8,8,0),(19,2,5,1),(19,4,6,1);
/*!40000 ALTER TABLE `student_assignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_courses`
--

DROP TABLE IF EXISTS `student_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_courses` (
  `stu_id` int(11) NOT NULL,
  `crs_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stu_id`,`crs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_courses`
--

LOCK TABLES `student_courses` WRITE;
/*!40000 ALTER TABLE `student_courses` DISABLE KEYS */;
INSERT INTO `student_courses` VALUES (1,2,0),(1,5,0),(1,8,0),(2,1,0),(2,4,0),(2,7,0),(2,8,0),(4,1,0),(4,3,0),(4,5,0);
/*!40000 ALTER TABLE `student_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `stu_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `DOB` varchar(11) DEFAULT NULL,
  `gender` int(2) DEFAULT NULL,
  PRIMARY KEY (`stu_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'pavan','pk@gmail.com','1996-07-14',1),(2,'john','hsn@gmail.com',NULL,1),(3,'dhoni','dhn@gmail.com',NULL,1),(4,'sindhu','sindhu@gmail.com',NULL,0),(5,'scarlet','scrlt@gmail.com',NULL,0),(6,'trumph','trmp@gmail.com',NULL,1),(7,'jessica','jsc@gmail.com',NULL,0),(8,'samul','sml@gmail.com',NULL,1),(9,'jesus','js@gmail.com',NULL,1),(10,'mahesh','msh@gmail.com',NULL,1),(11,'venkatesh','vnkt@gmail.com',NULL,0),(12,'sivaji','svj@gmail.com',NULL,1),(13,'sai','sai@gmail.com',NULL,1),(14,'sandhya','sandhya@gmail.com',NULL,0),(15,'daya','dya@gmail.com',NULL,1),(16,'shankar','snkr@gmail.com',NULL,1),(17,'lakshmi','lkshm@gmail.com',NULL,0),(18,'suraz','srz@gmail.com',NULL,1),(19,'maria','mr@gmail.com',NULL,0),(20,'flora','flr@gmail.com',NULL,0),(21,'jennifer','jnfr@gmail.com',NULL,0),(22,'sabrina','sbr@gmail.com',NULL,0);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24 10:39:27

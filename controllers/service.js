const db = process.env.NODE_ENV || 'development';
const MySql = require('../utils/dbConnectUtils')
const PromiseHandler = require('../utils/promiseHandler')
const ResponseUtils = require('../utils/responseUtils')

exports.getNewAssignments = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let courseId = apiReq.query.crs_id;
    let pageNo = apiReq.query.page_no;
    let pageRecords = apiReq.query.page_records || 10;
    let date = apiReq.query.date || new Date().toISOString().split('T')[0]

    if (!courseId || !pageNo) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'SELECT asn.name AS assignment_name, stf.name AS staff_name, start_date, end_date FROM assignments AS asn JOIN staff AS stf ON asn.stf_id = stf.stf_id WHERE asn.crs_id = ? AND submitted_date = ? LIMIT ?,?';
    let params = [courseId, date, ...getSkipLimit(pageNo, pageRecords)];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.getStudentCourses = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let studentId = apiReq.query.stud_id;
    let pageNo = apiReq.query.page_no;
    let pageRecords = apiReq.query.page_records || 10;

    if (!studentId || !pageNo) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'SELECT crs.crs_id AS course_id, crs.name course_name FROM student_courses stu JOIN courses_offered crs ON stu.crs_id = crs.crs_id WHERE stu.stu_id = ? order by crs.crs_id LIMIT ?,?';
    let params = [studentId, ...getSkipLimit(pageNo, pageRecords)];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.getStudents = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let staffId = apiReq.query.staff_id;
    let pageNo = apiReq.query.page_no;
    let pageRecords = apiReq.query.page_records || 10;

    if (!staffId || !pageNo) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'SELECT stu.name AS student_name FROM student_courses stu_crs JOIN staff stf ON JSON_CONTAINS(stf.courses, CONVERT(stu_crs.crs_id, CHAR)) = 1 JOIN students stu ON stu.stu_id = stu_crs.stu_id  WHERE  stf.stf_id = ? order by stu.stu_id LIMIT ?,?';
    let params = [staffId, ...getSkipLimit(pageNo, pageRecords)];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED');

    Response.success('', resp.data, 'SUCCESS');
}

exports.getCourses = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let pageNo = apiReq.body.page_no;
    let pageRecords = apiReq.body.page_records || 10;

    if (!pageNo) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'SELECT name FROM courses_offered order by crs_id LIMIT ?,?';
    let params = getSkipLimit(pageNo, pageRecords);

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.getAssignments = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let courseId = apiReq.body.course_id;
    let pageNo = apiReq.body.page_no;
    let pageRecords = apiReq.body.page_records || 10;

    if (!courseId || !pageNo) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'SELECT name, start_date, end_date FROM assignments WHERE crs_id = ? order by asn_id LIMIT ?,?';
    let params = [courseId, ...getSkipLimit(pageNo, pageRecords)];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.insertStudent = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let name = apiReq.body.name;
    let email = apiReq.body.email || '';
    let dob = apiReq.body.dob || null;
    let gender = apiReq.body.gender || null;

    if (!name) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'INSERT INTO students(name, email, DOB, gemder) VALUES(?,?,?,?)';
    let params = [name, email, dob, gender];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.insertStaff = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let name = apiReq.body.name;
    let email = apiReq.body.email || '';

    if (!name) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'INSERT INTO staff(name, email) VALUES(?,?)';
    let params = [name, email];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.insertCourses = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let name = apiReq.body.name;
    let startDate = apiReq.body.start_date;
    let endDate = apiReq.body.end_date;

    if (!name || !startDate || !endDate) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'INSERT INTO courses(name) VALUES(?)';
    let params = [name];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}

exports.createAssignment = async (apiReq, apiRes) => {
    let Response = new ResponseUtils.Response(apiRes);
    let name = apiReq.body.name;
    let courseId = apiReq.body.course_id;
    let staffId = apiReq.body.staff_id;
    let startDate = apiReq.body.start_date;
    let endDate = apiReq.body.end_date;

    if (!name || !courseId || !staffId || !startDate || !endDate) return Response.error('Invalid Parameters', 'BADPARAMS')

    let query = 'INSERT INTO assignments(name, crs_id, stf_id, start_date, end_date) VALUES(?,?,?,?,?)';
    let params = [name, courseId, staffId, startDate, endDate];

    let [err, resp] = await PromiseHandler(MySql.execSqlQry(query, params, db));
    if (err) return Response.error(err, 'FAILED')

    Response.success('', resp.data, 'SUCCESS');
}




const getSkipLimit = (page, records) => [(page - 1) * records, records]
const express = require('express')
const bodyparser = require('body-parser')
const app = express()


app.use(bodyparser.urlencoded({ extended: false }))

app.use(bodyparser.json())

const routes = require('./routes/routes')(app)

const port = process.env.PORT || 1528;
app.listen(port,()=>console.log(`nLearn application listening on ${port}`));